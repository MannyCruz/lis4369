> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Emmanuel Cruz

### P1 Requirements:

1. Requirements:

    a. Save as lis4369_p2.R. Include link to file in p2 README.md file.
    b. Include link to lis4369_p2_output.txt file
    c. Include at least one 4-panel RStudio screenshot executing lis4369_p2.R code.
    d. Also, be sure to include at least two plots (*must* include *your* name in plot titles), in
    your README.md file. See examples below.

2. Be sure to test your program using RStudio.


#### README.md file should include the following items:

* Screenshots of lis4369_p2 application running (RStudio)

#### Assignment Screenshots:



#### Screenshots of lis4369_p2 running

![Python Installation Screenshot Jupyter Notebook](img/p2_displacement_vs_mpg.png "p2 screenshot displacement vs mpg")

![Python Installation Screenshot Jupyter Notebook](img/p2_pie_chart.png "p2 screenshot pie chart")

![Python Installation Screenshot Jupyter Notebook](img/p2_weight.png "p2 screenshot weight and mpg")


#### Output file link Links:

[Project 2 Output file Link](ec_lis4369_p2_output.txt "Project 2 output")
