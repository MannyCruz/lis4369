> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions
#(Python/R Data Analytics/Visualization)

## Emmanuel Cruz

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Install Python
    * Install R
    * Install R Studio
    * Install Visual Studio Code
    * Create 'a1_tip_calculator' application
    * Create 'a1 tip calculator' Jupyter Notebook
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorial (bitbucketstationlocations)
    * Provide git command descriptions

2.  [A2 README.md](a2/README.md "My A2 README.md file")
    * Create "Payroll Calculator" application
    * Create "Payroll Calculator" Jupyter Notebook
    * Provide screenshots of running "Payroll Calculator" idle and visual studio code.
    * Provide screenshots of running ss1, ss2, ss3.
3.  [A3 README.md](a3/README.md "My A3 README.md file")
    * Create "Painting Estimator" application
    * Create "Painting Estimator" Jupyter Notebook
    * Provide screenshots of running "Painting Estimator" idle.
    * Provide screenshots of running ss4, ss5, ss6
4.  [A4 README.md](a4/README.md "My A4 README.md file")
    * Create "Data Analysis 2" application
    * Create "Data Analysis 2" Jupyter Notebook
    * Provide screenshots of running "Data Analysis 2" idle.
    * Provide screenshots of running ss10, ss11, ss12
5.  [A5 README.md](a5/README.md "My A5 README.md file")
    * Code and run the "Learn How to use R" in the RStudio environment.
    * Code and run the demo1.R and demo2.R into one .R file.
    * Provide screenshots of 2 graphs based off of "learn_to_use_r.R".
    * Provide screenshots of 2 graphs based off of "lis4369_a5.R".
6.  [P1 README.md](p1/README.md "My P1 README.md file")
    * Create "Data Analysis 1" application
    * Create "Data Analysis 1" Jupyter Notebook
    * Provide screenshots of running "Data Analysis 1" idle and Jupyter Notebook
    * Provide screenshots of running ss7, ss8, ss9.
7.  [P2 README.md](p2/README.md "My P2 README.md file")
    * Create "lis4369_p2" using RStudio
    * Provide screenshot of piechart.
    * Provide screenshot of displacement vs mpg.
    * Provide screenshot of relationship weight and mpg.




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
