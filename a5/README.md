> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4369

## Emmanuel Cruz

### Assignment 5 Requirements:

1. Requirements:

    a.) Code and run demo.py.(Note: *be sure* necessary packages are installed!)  
    b.) Use it to backward-engineer the screenshots below it.
    c.) Go through "Learn How to Use R" and code demo1.R and demo2.R to create a5.

#### README.md file should include the following items:

* Screenshots of assignment 5 running in RStudio environment.
* Screenshots of skillsets 13-15


#### Assignment Screenshots:

![Python Installation Screenshot Jupyter Notebook](img/lis4369_a5_how_to_r.png "a5 screenshot how to use r")

![Python Installation Screenshot Jupyter Notebook](img/lis4369_a5_bargraph_howtor.png "a5 screenshot how to use r bargraph")

![Python Installation Screenshot Jupyter Notebook](img/lis4369_a5_scatterplot_howtor.png "a5 screenshot how to use r scatterplot")

![Python Installation Screenshot Jupyter Notebook](img/lis4369_a5_titanic.png "a5 titanic")

![Python Installation Screenshot Jupyter Notebook](img/lis4369_a5_titanic_bargraph.png "a5 titanic bargraph")

![Python Installation Screenshot Jupyter Notebook](img/lis4369_a5_scatterplot.png "a5 titanic scatterplot")

#### SS13 Screenshot

![Python Installation Screenshot Jupyter Notebook](img/lis4369_ss13.png "ss13")

#### SS14 Screenshot

![Python Installation Screenshot Jupyter Notebook](img/lis4369_ss14.png "ss14")

#### SS15 Screenshot

![Python Installation Screenshot Jupyter Notebook](img/lis4369_ss15.png "ss15")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
