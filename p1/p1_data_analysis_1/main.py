#!/usr/bin/env python3

#Developer: Emmanuel Cruz

import functions as f
import demo as d

def main():
    f.get_requirements()
    d.data_analysis_1()

if __name__=="__main__":
    main()