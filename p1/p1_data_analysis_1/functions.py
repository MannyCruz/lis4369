#!/usr/bin/env python3

#Developer: Emmanuel Cruz

def get_requirements():
    print("Data Analysis 1")
    print("\nProgram Requirements:\n"
    +"1. Run demo.py.\n"
    +"2. If errors, more than likely missing installations.\n"
    +"3. Test Python Package Installer: pip freeze.\n"
    +"4. Research how to do the following installations: \n"
    +"\ta. pandas (only if missing).\n"
    +"\tb. pandas data-reader (only if missing).\n"
    +"\tc. matplotlib (only if missing).\n"
    +"5. Create at least three functions that are called by the program:\n"
    +"\ta. main(): calls at least two other functions.\n"
    +"\tb. get_requirements(): displays the program requirements.\n"
    +"\tc. data_analysis_1(): displays the following data.\n")


