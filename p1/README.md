> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Emmanuel Cruz

### P1 Requirements:

1. Requirements:

    a.) Code and run demo.py.(Note: *be sure* necessary packages are installed!)  
    b.) Use it to backward-engineer the screenshots below it.  
    c.) Update conda, and install necessaryPython packages.Invoke commands below.  
    &nbsp;&nbsp;Log in as administrator(PC:Anaconda command prompt, LINUX/Mac:sudo)  
    a.) conda update conda  
    b.) conda update --all  
    c.) pip install pandas-datareader  

2. Be sure to test your program using both IDLE and Visual Studio Code.

#### README.md file should include the following items:

* Screenshot of p1_data_analysis application running (IDLE and Jupyter Notebook)
* Link to P1 .ipynb file: [data_analysis.ipynb](p1_data_analysis_1/data_analysis.ipynb "P1 Jupyter Notebook")

#### Assignment Screenshots:

#### Screenshot of p1_data_analysis running (IDLE)

![Python Installation Screenshot Jupyter Notebook](img/p1_data_analysis_full.png "p1 screenshot full view")

#### Screenshot of p1_data_analysis graph

![Python Installation Screenshot Jupyter Notebook](img/p1_data_analysis_graph.png "p1 screenshot graph")

#### Screenshots of p1_data_analysis running (jupyter notebook)

![Python Installation Screenshot Jupyter Notebook](img/p1_data_analysis_jupyter_1.png "p1 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/p1_data_analysis_jupyter_2.png "p1 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/p1_data_analysis_jupyter_3.png "p1 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/p1_data_analysis_jupyter_4.png "p1 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/p1_data_analysis_jupyter_5.png "p1 screenshot jupyter notebook")


#### SS7 Screenshot

![Python Installation Screenshot Jupyter Notebook](img/ss7_using_lists.png "SS7 screenshot")

#### SS8 Screenshot

![Python Installation Screenshot Jupyter Notebook](img/ss8_using_tuples.png "SS8 screenshot")

#### SS9 Screenshot

![Python Installation Screenshot Jupyter Notebook](img/ss9_using_sets_1.png "SS9 screenshot")
![Python Installation Screenshot Jupyter Notebook](img/ss9_using_sets_2.png "SS9 screenshot")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
