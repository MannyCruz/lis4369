> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Emmanuel Cruz

### Assignment 2 Requirements:

1. Bitbucket repo links:

#### README.md file should include the following items:

* Screenshot of a2_payroll application running
* Link to A2 .ipynb file: [payroll.ipynb](a2_payroll/payroll.ipynb "A2 Jupyter Notebook")
*

#### Assignment Screenshots:

#### Screenshot of a2_payroll running (IDLE):

![Python Installation Screenshot IDLE](img/a2_payroll_idle.png "A2 IDLE screenshot")

#### A2 Jupyter Notebook:

![payroll.ipynb](img/a2_jupyter_notebook.png "A2 Jupyter Notebook")

#### SS1 Screenshot

![payroll.ipynb](img/ss1_sqft_to_acres.png "A2 SS1")

#### SS2 Screenshot

![payroll.ipynb](img/ss2_mpg.png "A2 SS2")

#### SS3 Screenshot

![payroll.ipynb](img/ss3_it_ict_percent.png "A2 SS2")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
