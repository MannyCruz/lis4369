#!/usr/bin/env python3

#Developer: Emmanuel Cruz
#Course LIS4369

def get_requirements():
    print("Paint Estimator")
    print("Dev: Emmanuel Cruz")
    print("\nProgram Requirements:\n"
        + "1. Calculate home interior paint cost.\n"
        + "2. Must use float data types.\n"
        + "3. Must use SQFT_PER_GALLON constant (350).\n"
        + "4. Must use iteration structure (aka loop)\n"
        + "5. Format, right-align numbers, and round to two decimal places.\n"
        + "6. Create at least 5 functions that are called by the program:\n"
        + "\ta. main() calls two other functions: get_requirements() and estimate_painting_cost().\n"
        + "\tb. print_painting_estimate(): displayes painting cost.\n"
        + "\tc. print_painting_estimate(): displays painting cost.\n"
        + "\td. print_painting_percentage(): displays printing cost percentages.\n")

def estimate_painting_cost():
    SQFT_PER_GALLON = 350
    while True: 
        print("\nInput")
        total_interior = float(input('Enter total interior square feet: '))
        price_per_gallon = float(input('Enter the price for each gallon of paint '))
        hourly_rate = float(input('Enter hourly rate for painting per square foot '))

        number_of_gallons = total_interior / SQFT_PER_GALLON
        cost_of_paint = number_of_gallons * price_per_gallon
        cost_of_labor = total_interior * hourly_rate

        total_cost = cost_of_paint + cost_of_labor
        paint_percentage = cost_of_paint / total_cost * 100
        labor_percentage = cost_of_labor / total_cost * 100
        total_percentage = paint_percentage + labor_percentage

        print_painting_estimate(total_interior, SQFT_PER_GALLON, number_of_gallons, price_per_gallon, hourly_rate)
        print_painting_percentage(cost_of_paint, cost_of_labor, total_cost, paint_percentage, labor_percentage, total_percentage)

        cont = input("\n\nEstimate another paint job? (y/n): ")
        while cont.lower() not in ('y', 'n'):
            cont = input ('Estimate another paint job? (y/n): ')
        if cont == 'n': 
            print("Thanks for using the paint estimator!")
            break

def print_painting_estimate(total_interior, SQFT_PER_GALLON, number_of_gallons, price_per_gallon, hourly_rate):
    print("\nOutput: ")
    print("{0:<10} {1:>20}".format('Item', 'Amount'))
    print("{0:<10} {1:>17,.2f}".format('Total Sq Ft: ', total_interior))
    print("{0:<10} {1:>13,.2f}".format('Sq Ft per gallon:', SQFT_PER_GALLON))
    print("{0:<10} {1:>12,.2f}".format('Number of gallons:', number_of_gallons))
    print("{0:<10}     ${1:>8,.2f}".format('Paint per gallon:', price_per_gallon))
    print("{0:<10}      ${1:>8,.2f}".format('Labor per Sq Ft:', hourly_rate))

def print_painting_percentage(cost_of_paint, cost_of_labor, total_cost, paint_percentage, labor_percentage, total_percentage):
    print("{0:<9} {1:>10} {2:>14}".format('\nCost', 'Amount', 'Percentage'))
    print("{0:<9} ${1:>8,.2f} {2:>13.2f}%".format('Paint', cost_of_paint, paint_percentage))
    print("{0:<9} ${1:>4,.2f} {2:>13.2f}%".format('Labor', cost_of_labor, labor_percentage))
    print("{0:<9} ${1:>4,.2f} {2:>13.2f}%".format('Total', total_cost, total_percentage))

