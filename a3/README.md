> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Emmanuel Cruz

### Assignment 3 Requirements:

1. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of a3_painting_estimator application running (IDLE and Jupyter Notebook)
* Link to A3 .ipynb file: [painting_estimator.ipynb](a3_painting_estimator/painting_estimator.ipynb "A3 Jupyter Notebook")

#### Assignment Screenshots:

#### Screenshot of a3_painting_estimator running (IDLE)

![Python Installation Screenshot IDLE](img/a3_painting_estimator_idle.png "A3 IDLE Screenshot")

#### Screenshot 1 of a3_painting_estimator running (jupyter notebook)

![Python Installation Screenshot Jupyter Notebook](img/a3_jupyter_notebook.png "A3 Jupyter Notebook Screenshot")

#### Screenshot 2 of a3_painting_estimator running (jupyter notebook)

![Python Installation Screenshot Jupyter Notebook](img/a3_jupyter_notebook_2.png "A3 Jupyter Notebook Screenshot")

#### SS4 Screenshot

![painting_estimator.ipynb](img/ss4_calorie_percentage.png "A3 SS4")

#### SS5 Screenshot

![painting_estimator.ipynb](img/ss5_python_selection_structures.png "A3 SS5")

#### SS6 Screenshot

![painting_estimator.ipynb](img/ss6_python_loops.png "A3 SS6")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
