#!/usr/bin/env python3

import functions as f

f.get_requirements()
print("\nInput:")
x = int(input("Enter beginning value: "))
y = int(input("Enter end value: "))
y += 1

print("\nOutput:")
print("Example 1")
f.example_1(x,y)
print("\nExample 2:")
f.example_2(x,y)