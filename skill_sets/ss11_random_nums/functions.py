#!/usr/bin/env python3

import random
from typing import MutableMapping

def get_requirements():
    print("Dev Emmanuel\n")
    print("Pseudo-Random Number Generator\n")
    print("1. Get user beginning and ending values")
    print("2. Display 10 pseudo-random numbers between and including values")
    print("3. Must use int data types")
    print("4. Example 1: using range() and randint()")
    print("5. Example 2: using a list with range() and shuffle()")

def example_1(beg, end):
    ran = range(beg, end)
    for x in ran:
        y = (random.randint(beg, end))
        print(y, end = " ")

def example_2(beg, end):
    ran = range(beg, end)
    list = []
    for x in ran:
        list.append(x)
    random.shuffle(list)
    for x in list:
        print(x, end = " ")