#!/usr/bin/env python3

# Developer: Emmanuel Cruz

import math as m 

def get_requirements():
    print("Dev: Emmanuel Cruz")
    print("Sphere Volume Calculator")
    print("\nProgram Requirments:\n"
    + "1. Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches, and rounds to two decimal places.\n"
    + "2. Must use Python's *built in* PI and pow() capabilities.\n"
    + "3. Programs checks for non-integers and non-numeric values.\n"
    + "4. Program continues to prompt for user entry until no longer requested, prompt accepts upper or lower case letter.\n")


def calculate():
    pi = 3.14159265;
    di = 0.0
    frac = 1.33333;
    gal = 19.25317;
    choice = ' '
    ans = 0;

    print("Input: ")
    choice = input("Would you like to use the sphere calculator? y or n.\n").lower()

    print("Output: ")

    while (choice == 'y'):
        while True:
            try:
                dia = int(input("Please enter diameter in inches (integers only): "))
                dia = dia*.5

                ans = (((m.pow(dia,3))*frac*pi)/12)/gal
                ans = str(round(ans,2))
                print("Sphere volume: " + ans + " liquid u.s. gallons.")

                choice = input("Would you like to use the sphere calculator? y or n.\nn")

            except ValueError: 
                print("Not an integer: Try again.")
                continue
            else: 
                break
        else: 
            print("Adios!")
            


