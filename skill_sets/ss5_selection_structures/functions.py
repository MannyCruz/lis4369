#!/usr/bin/env python3

#Developer: Emmanuel Cruz
#Course: LIS4369
#Semester: FALL 2021

#Program Requirements:

def get_requirements():
    print("Python Selection Structures")
    print("\nProgram requirements:\n"
        +" 1. Use python selection structure\n"
        +" 2. Prompt user for two numbers, and a suitable operator\n"
        +" 3. Test for correct numeric operator\n"
        +" 4. Replicate display below\n")


def get_input():
# get number and operator from user
    print("Python Calculator")
    num1 = input("Enter num1: ")
    num2 = input("Enter num2: ")

    print("Suitable operators: +, -, *, /, // (integer division), % (modulo operator), ** (power)")
    operator = input("Enter operator: ")

# check to see if input is valid

    if num1.isnumeric() == True and num2.isnumeric() == True:
        n1 = float(num1)
        n2 = float(num2)
        operate(n1, n2, operator)
    else: 
        print("input is invalid")

def operate(num1, num2, operator):
# check to see if operator is valid and perform the operation between the two float numbers

    if operator == "+":
        num3 = num1 + num2
        print (num3)
    elif operator == "-":
        num3 = num1 - num2
        print(num3)
    elif operator == "*":
        num3 = num1 * num2
        print(num3)
    elif operator == "/":
        if(num2 == 0):
            print("can't divide by 0")
        else:
            num3 = num1 / num2
            print(num3)
    elif operator == "//":
            if(num2 == 0):
                print("can't divide by 0")
            else:
                num3 = num1 // num2
                print(num3)
    elif operator == "%":
        if(num2 == 0):
            print("can't divide by 0")
        else:
            num3 = num1 % num2
            print(num3)
    elif operator == "**":
        num3 = num1 ** num2
        print(num3)
    else:
        print("illegal operator!!!")



