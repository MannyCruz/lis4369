#!/usr/bin/env python3

def get_requirements_2():
    print("Trying to get a hang of this programming thing")

def calc_mpg_2():
    print("Input")
    miles = float(input("Enter the miles driven: "))
    gals = float(input("Enter the amount of galons used: "))

    mpg = miles / gals

    print("This is your miles per gallon ratio: ", mpg)
    print("{0:,.2f} {1} {2:,.2f} {3} {4:,.2f} {5}".format(miles, "miles per", gals, "gallons gives you", mpg, "mpg"))
    