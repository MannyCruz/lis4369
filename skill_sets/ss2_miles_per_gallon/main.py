#!/usr/bin/env python3

# 2021-09-17
# Emmanuel Cruz
# ec20es@my.fsu.edu

import functions as f

def main():
    f.print_requirements()
    f.calc_mpg()

if __name__=="__main__":
    main()

    