#!/usr/bin/env python3 

# number of square feet in an acre = 43560

SQFT_PER_ACRE = 43560

def get_requirements_2():

    print("\nHello, these are just some mach requirments for my own practice.\n"
    + "I want"
    + " to see"
    + " what I can program myself.\n")

def calculate_sqft_to_acre_2():
    
    # initialization 

    tract_size = 0.0
    acres = 0.0 

    # get square feet in tract
    print("SQFT to acreage converter")

    print("Input ")
    tract_size = float(input("Enter square feet: "))

    # calculate acreage

    acres = tract_size/SQFT_PER_ACRE

    # print number of acres 

    print("Output ")
    print("{0:,.2f} {1} {2:,.2f} {3}".format(tract_size, "square feet =", acres, "acres"))