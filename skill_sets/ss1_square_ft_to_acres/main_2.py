#!/usr/bin/env python3

import functions_2 as f

def main():
    f.get_requirements_2()
    f.calculate_sqft_to_acre_2()

if __name__=="__main__":
    main()