#!/usr/bin/env python3

import os

def get_requirements():
    print("Dev: Emmanuel Cruz")
    print("\nCreate write read file subdirectory with two files: main and functions\n"
    + "2. Use Abe Lincoln's gettysburg address full address\n"
    + "3. Write address to file\n"
    + "4. Read address from same file.\n"
    + "5. Create Python Docstrings for functions.py file\n")

def file_write():

    f=open("tests.txt","w")
    f.write("\nPresident Abraham Lincoln's Gettysburg address:/n Four score\n"
    + "\nand seven years ago our fathers brought forth, upon this continent\n"
    + "\na new nation, conceived in liberty and dedicated to the proposition\n"
    + "\nthat all men are created equal. Now we are engaged in a great civil\n"
    + "\ntesting whether that nation or any nation so conceived, and so dedicated/n"
    + "\nand can long endure. We are met on a great battle field of that war. We have\n"
    + "\ncome to dedicate a portion of that field, as a final resting place for those who here\n"
    + "\ngave their lives that that nation might live. It is altogether fitting and proper that we should do this.\n"
    + "\nBut, in a larger sense, we can not dedicate -- we can not consecrate -- we can not hallow -- this\n"
    + "\nground. The brave men, living and dead, who struggled here, have consecrated it, far above our poor power to\n"
    + "\nadd or detract. The world will little note, nor long remember what we say here, but it can never forget what they did here.\n" 
    + "\nIt is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly\n" 
    + "\nadvanced. It is rather for us to be here dedicated to the great task remaining before us -- that from these honored dead we take increased/n"
    + "\ndevotion to that cause for which they gave the last full measure of devotion -- that we here highly resolve that these dead shall not have died\n" 
    + "\nin vain -- that this nation, under God, shall have a new birth of freedom -- and that government of the people, by the people, for the people, shall\n"
    + "\nnot perish from the earth.")
    f.write("\n\Full File Path:")
    f.write(os.path.realpath(f.name))
    f.close

def file_read():
        f = open("tests.txt","r")
        print(f.read())

def write_read_file():
        file_write()
        file_read()