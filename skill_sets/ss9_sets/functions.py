#!/usr/bin/env python3

def get_requirements():
    print("\nPython Sets - like mathematical sets!\n")
    print("\nProgram requirements:\n"
    +"1. Sets (Python data structure): mutable, heterogeneous, unordered sequence of elements, *but* cannot hold duplicate values.\n"
    +"2. Sets are mutable/changeable--that is, can insert, update, delete\n"
    +"3. While sets are mutable/changeable *cannot* contain other mutable items like list, set, or dictionary\n"
    +"\tthat is, elements contained in set must be immutable.\n"
    +"4. Also, since sets are unordered, cannot use indexing or slicing to access, update, or delete elements.\n"
    +"5. Two methods to create sets: \n"
    +"\ta. create set using curly brackets {set}: my_set={1, 3.14, 2.0, 'four', 'five'}\n"
    +"\tb. create set using set() function my_set = set(<iterable>)\n"
    +"\tExamples:\n"
    +"\tmy_set1 = set([1, 3.14, 2.0, 'four', 'five']) #set with list\n"
    +"\tmy_set2 = set((1, 3.14, 2.0, 'four', 'five')) #set with tuple\n"
    +"5. Note: an iterable is any object which can be iterated over--that is, lists, tuples, or even strings.\n"
    +"6. Creat a program that mirrors the following IPO (input/process/output) format.\n"
    )

def using_sets():
    print("Input: hard coded--no user input. See three examples above.")
    print("***Note***: All three sets below print as 'sets' (i.e. curly brackets), regardless of how they were created!\n")
    print()
    
    my_set= {1, 3.14, 2.0, 'four', 'five'}
    print("Print my_set created using curly brackets: ")
    print(my_set)
    print()

    print("my_set type: ")
    print(type(my_set))
    print()

    my_set1 = set([1, 3.14, 2.0, 'four', 'five'])
    print("Print my_set1 created using set() function with list: ")
    print(my_set1)
    print()

    print("my_set1 type: ")
    print(type(my_set1))
    print()

    my_set2 = set((1, 3.14, 2.0, 'four', 'five'))
    print("Print my_set2 created using set() function with tuple: ")
    print(my_set2)
    print()

    print("my_set2 type: ")
    print(type(my_set2))
    print()

    print("length of my_set")
    print(len(my_set))
    print()

    print("Discard 'four'")
    my_set.discard('four')
    print()

    print("Remove 'five'")
    my_set.remove('five')
    print()

    print("Length of my_set: ")
    print(len(my_set))
    print()

    print("Add element to set (4) using add () method: ")
    my_set.add(4)
    print(my_set)
    print()

    print("length of my_set:")
    print(len(my_set))
    print()

    print("Display minimum number")
    print(min(my_set))
    print()

    print("Display maximum number: ")
    print(max(my_set))
    print()

    print("Display sum of numbers")
    print(sum(my_set))
    print()

    print("Delete all set elements ")
    my_set.clear()
    print(my_set)
    print()

    print("Length of my_set: ")
    print(len(my_set))
    print()
