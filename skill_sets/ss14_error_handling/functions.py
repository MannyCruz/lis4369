#!/usr/bin/env python3

def get_requirements():
    print("\nDev: Emmanuel Cruz\n")
    print("\nError Handling Calculator\n")
    print("1. Program calculates two numbers, and rounds to two decimal places.\n"
    + "2. Prompt user for two numbers, and a suitable operator.\n"
    + "3. Use Python for error handling to validate data.\n"
    + "4. Test for correct arithmetic operator.\n"
    + "5. Division by 0 not permitted.\n"
    + "6. Note: program loops until correct input entered - numbers and arithmetic operator.\n"
    + "7. Replicate display below.\n")

def getNum(prompt):
    while True:
        try: 
            return float(input("\n " + prompt + " "))
        except ValueError:
            print("INvalid, try again")

def getOp():
    validOperators = ["+", "-", "*", "/", "//","**","%"]
    while True:
        op = input("Suitable ops are + - * / // ** ")
        try:
            validOperators.index(op)
            return op
        except ValueError:
            print("NO")
def calc():

    num1 = getNum("Enter num1:")
    num2 = getNum("Enter num2:")
    op = getOp()
    sum = 0.0

    if op == '+':
        sum = num1 + num2
    elif op == '-':
        sum = num1 - num2
    elif op == '*':
        sum = num1 * num2
    elif op == '/':
        sum = num1 / num2
    elif op == '//':
        sum = num1 // num2
    elif op == '**':
        sum = num1 ** num2
    elif op == '%':
        while True:
            try: 
                sum = num1 % num2
                break 
            except ZeroDivisionError:
                num2 = getNum("No divison by zero")
    elif op == '/':
        while True:
            try: 
                sum = num1 / num2
                break
            except ZeroDivisionError:
                num2 = getNum("No division by zero")
    elif op == '//':
        while True:
            try: 
                sum = num1 // num2
                break
            except ZeroDivisionError:
                num2 = getNum("No division by zero")

    print("\nAnswer: " + str(round(sum,2)))


    