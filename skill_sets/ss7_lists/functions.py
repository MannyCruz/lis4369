#!/usr/bin/env python3

#Developer: Emmanuel Cruz

def get_requirements():
    print("\nDeveloper Emmanuel Cruz\n")
    print("Python Lists")
    print("\nProgram Requirements:\n"
    +"1. Lists (Python data structure): mutable, ordered, sequence of elements.\n"
    +"2. Lists are mutable/changeable--that is, can insert, update, delete.\n"
    +'3. Create list - using square brackets [list]: my_list = ["cherries", "apples", "bananas", "oranges"].\n'
    +"4. Create a program that mirrors the following IPO (input/process/output) format.\n"
    +"Note: User enters number of requested list elements, dynamically rendered below (that is, number of elemements can change each run).\n"
    )

def user_input():
    
    num = 0

    print("Input:")
    num = int(input("Enter number of list elements: "))
    return num

print() # prints blank line

def using_lists(num):
    my_list = []

    for i in range(num):

        my_element = input('Please enter list element ' + str(i + 1)+": ")
        my_list.append(my_element)

    print("\nOutput:\n")
    print("Print my_list:")
    print(my_list)

    elem = input("Please enter list element:")
    pos = int(
        input("Please enter list *index* position (note: must convert to int):"))

    print("\nInsert element into specific position in my_list")
    my_list.insert(pos,elem)
    print(my_list)

    print("\nCount number of elements in list: ")
    print(len(my_list))

    print("\nSort elements in list alphabetically: \n")
    my_list.sort()
    print(my_list)

    print("Reverse the list: \n")
    my_list.reverse()
    print(my_list)

    print("Remove last list element: \n")
    my_list.pop()
    print(my_list)

    print("Delete second element from list by *index* (note: 1 = second element): \n")
    del my_list[1]
    print (my_list)

    print("Delete element from list by *value* (cherries): \n")
    my_list.remove('cherries')
    print(my_list)

    print("Delete all elements from list: ")
    my_list.clear()
    print(my_list)

