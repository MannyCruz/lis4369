#!/usr/bin/env python3

# Developer: Emmanuel Cruz
# 2021-09-17
# LIS4369 

def get_requirements():
    print("IT/ICT Student Percentage\n")
    print("Program Requirements:\n"
            + "1. Find number of IT/ICT students in class.\n"
            + "2. Calculate IT/ICT student percentage.\n"
            + "3. Format, right-align numbers, and round to two decimal places.")

def calc_percent():
    print("Input: ")
    it_stud = float(input("Enter number of IT students: "))
    ict_stud = float(input("Enter number of ICT students: "))

    total_stud = it_stud + ict_stud
    it_percent = (it_stud/total_stud) * 100
    ict_percent = (ict_stud/total_stud) * 100

    print ("\noutput:")
    print(f'Total Students: {total_stud:>8.2f}')
    print(f'IT Students: {it_percent:>10.2f}%')
    print(f'ICT Students: {ict_percent:>9.2f}%\n')
