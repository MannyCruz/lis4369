#!/usr/bin/env python3

def get_requirements_2():
    print("\nStill trying to get the hang of this, "
    + "making progress though!\n")

def calc_percent_2():
    it_stud_2 = float(input("Enter the number of it students: "))
    ict_stud_2 = float(input("Enter the number of ict students: "))

    tot_stud = it_stud_2 + ict_stud_2
    it_perct = (it_stud_2 / tot_stud) * 100
    ict_perct = (ict_stud_2 / tot_stud) * 100

    print(f"Total students: {tot_stud:>8.2f}")
    print(f"IT percent: {it_perct:>12.2f}")
    print(f"ICT percent: {ict_perct:>11.2f}")
