#!/usr/bin/env python3

# Developer: Emmanuel Cruz
# 2021-09-17
# LIS4369 

import functions as f

def main():
    f.get_requirements()
    f.calc_percent()

if __name__=="__main__":
    main()