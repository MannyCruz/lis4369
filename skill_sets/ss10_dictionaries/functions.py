#!/usr/bin/env python3

def get_requirements():
    print("Python Dictionaries: \n")
    print("\nProgram requirements: \n"
    + "1. Dictionaries (Python data structure): unordered: key value pairs.\n"
    + "2. Dictionary: an associative array also known as hashes.\n"
    + "3. Any key in a dictionary ia asscoiated (or mapped) to a value (any Pythonn data type).\n"
    + "4. Keys must be of immutable type and must be unique.\n"
    + "5. Values can be any data type and can repeat.\n"
    + "6. Create a program that mirrors the following IPO format.\n"
    + "\t Create empty dictionary using curly braces: my_dictionary = {}\n"
    + "\t Use the following keys: fname, lname, degree, major, gpa\n"
    + "\t Note: dictionaries have key-value pairs instead of single values.\n")

def using_dictionaries():
    # initialize variables
    v_fname=""
    v_lname=""
    v_degree=""
    v_major=""
    v_gpa=0.0
    my_dictionary={} # creates empty dictionary

    # IPO and get user data
    print("Input: ")
    # note 'v_' indicates varibale
    v_fname=input("First name: ")
    v_lname=input("Last name: ")
    v_degree=input("Degree: ")
    v_major=input("Major (IT or ICT): ")
    v_gpa=float(input("GPA: "))
    my_dictionary={}

    print()

    # process (note: key names)
    my_dictionary['fname'] = v_fname
    my_dictionary['lname'] = v_lname
    my_dictionary['degree'] = v_degree
    my_dictionary['major'] = v_major
    my_dictionary['gpa'] = v_gpa

    # Output:
    print("\nOutput: ")
    print("Print my_dictionary: ")
    print(my_dictionary)

    print("\nReturn value of dictionary's (key, value) pair; built in function:")
    print(my_dictionary.items())

    print("\nReturn view object of all keys, built in function:")
    print(my_dictionary.values())

    print("\nPrint only first and last names using keys: ")
    print(my_dictionary['fname'], my_dictionary['lname'])

    # get() function returns value of key
    print("\nPrint only first and last names using get() function:")
    print(my_dictionary.get('fname'), my_dictionary.get('lname'))

    print("\nCount number of items (key: value pairs) in dictionary: ")
    print(len(my_dictionary))

    print("\nRemove last dictionary item (popitem): ")
    my_dictionary.popitem()
    print(my_dictionary)

    print("\nDelete major from dictionary")
    my_dictionary.pop('major')
    # or del my_dictionary['major']
    print(my_dictionary)

    print("\nReturn object type: ")
    print(type(my_dictionary))

    print("\nDelete all items from list: ")
    my_dictionary.clear()
    print(my_dictionary)

    # del my_dictionary
