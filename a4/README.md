> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4369

## Emmanuel Cruz

### Assignment 4 Requirements:

1. Requirements:

    a.) Code and run demo.py.(Note: *be sure* necessary packages are installed!)  
    b.) Use it to backward-engineer the screenshots below it.
    c.) When displaying the required graph (see code below), answer the following question:
        Why is the graph line split?

#### README.md file should include the following items:

* Screenshot of a4_data_analysis_2 application running (IDLE and Jupyter Notebook)
* Link to a4 .ipynb file: [data_analysis_2.ipynb](a4_data_analysis_2/data_analysis_2.ipynb "A4 Jupyter Notebook")


#### Assignment Screenshots:

![Python Installation Screenshot Jupyter Notebook](img/a4_jupyter_notebook_1.png "a4 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/a4_jupyter_notebook_2.png "a4 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/a4_jupyter_notebook_3.png "a4 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/a4_jupyter_notebook_4.png "a4 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/a4_jupyter_notebook_5.png "a4 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/a4_jupyter_notebook_6.png "a4 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/a4_jupyter_notebook_7.png "a4 screenshot jupyter notebook")

![Python Installation Screenshot Jupyter Notebook](img/a4_jupyter_notebook_8.png "a4 screenshot jupyter notebook")

#### SS10 Screenshot

![Python Installation Screenshot Jupyter Notebook](img/ss10_using_dictionaries.png "ss10")

#### SS11 Screenshot

![Python Installation Screenshot Jupyter Notebook](img/ss11_pseudo_random_num_gen.png "ss11")

#### SS12 Screenshot

![Python Installation Screenshot Jupyter Notebook](img/ss12_temp_convert.png "ss12")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
