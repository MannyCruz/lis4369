def demo():    
    import re
    import numpy as np
    np.set_printoptions(threshold=np.inf)
    import pandas as pd
    import matplotlib.pyplot as plt

    url = "https://raw.github.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv"
    df = pd.read_csv(url)

    print("***DataFrame composed of three components: index, columns, and data. Data also known as values.***")
    index = df.index
    columns = df.columns
    values = df.values

    print("\n1. Print indexes:")
    print(index)

    print("\n2. Print columns")
    print(columns)

    print("\n3 Print columns (another way)")
    print(df.columns[:])

    print("\n4. Print (all) values, in array format:")
    print(values)

    print("\n5. ***Print component data types:***")
    print("\n\tIndex type:")
    print(type(index))
    print("\n\tColumns type:")
    print(type(columns))
    print("\n\tValues type:")
    print(type(values))

    print("\n6. Print summary of DataFrame (similar to 'describe tablename;' in MySQL):") 
    print(df.info())

    print("\n7. First five lines (all columns)") 
    print(df.head())

    df = df.drop('Unnamed: 0', 1)
    print("\n8. Print summary of DataFrame (after dropping column Unnamed") 
    print(df.info())

    print("\n9. First five lines (after dropping column 'Unnamed: 0')")
    print(df.head())

    print("\n***Precise data selection (data slicing)***")
    print("\n10. Using iloc, return first 3 rows")
    print(df.iloc[:3])

    print("\n11. Using iloc, return last 3 rows")
    print(df.iloc[1310:])

    print("\n12. Select rows 1, 3, and 5 and columns 2, 4, and 6")
    a = df.iloc[[0,2,4], [1,3,5]]
    print(a)

    print("\n13. Select all rows and columns 2, 4, and 6")
    a = df.iloc[:, [1,3,5]]
    print(a)

    print("\n14. Select rows 1, 3, 5 an all columns")
    a = df.iloc[[2,4,6], :]
    print(a)

    print("\n15. Select all rows and all columns")
    a = df.iloc[:, :]
    print(a)

    print("\n16. Select all rows and all columns starting at column 2")
    a = df.iloc[:, 1:]
    print(a)

    print("\n17. Select row 1 and column 1")
    a = df.iloc[0:1,0:1]
    print(a)

    print("\n18. Select rows 3-5 and columns 3-5")
    a = df.iloc[2:5,2:5]
    print(a)

    print("\n19. ***Convert pandas DataFrame df to NumPy ndarray using values command***")
    b = df.iloc[:, 1:].values

    print("\n20. Print data frame type")
    print(type(df))

    print("\n21. Print a type")
    print(type(a))

    print("\n22. Print b type")
    print(type(b))

    print("\n23. Print number of dimensions and items in array")
    print(b.shape)

    print("\n24. Print type of items in array")
    print(b.dtype)

    print("\n25. Printing a")
    print(a)

    print("\n26. Length a")
    print(len(a))

    print("\n27. Printing b")
    print(b)

    print("\n28. Length b")
    print(len(b))

    print("\n29. Print element of ndarray b in second row, third column")
    print(b[1,2])

    print("\n30. Print all record for NumPy array column 2")
    print(b[:,1])

    print("\n31. Get passenger names")
    names = df["Name"]
    print(names)

    print("\n32. Find all passengers with the name \"Allison\" using regular expressions")
    for name in names:
        print(re.search(r'(Allison)', name))

    print("\n33. Stat. Analysis")
    print("\ta. Print mean age")
    avg = df["Age"].mean()
    print("\t\t" + str(avg))
    print("\tb. Print mean age rounded to two decimal places")
    avg = df["Age"].mean()
    avg = str(round(avg, 2))
    print("\t\t" + avg)
    print("\tc. Print mean of every column")
    avg_all = df.mean(axis=0)
    print("\t\t" + str(avg_all))
    print("\td. Print summary stats")
    describe = df['Age'].describe()
    print(describe)
    print("\te. Print minimum age")
    min = df["Age"].min()
    print("\t\t" + str(min))
    print("\tf. Print max age")
    max = df["Age"].max()
    print("\t\t" + str(max))
    print("\tg. Print median age")
    median = df["Age"].median()
    print("\t\t" + str(median))
    print("\th. Print mode age")
    mode = df["Age"].mode()
    print("\t\t" + str(mode))
    print("\ti. Print number of values")
    count = df["Age"].count()
    print("\t\t" + str(count))

    print("Graph: display ages of the first 20 passengers")
    ages = df["Age"].head(20)
    ages.plot()
    plt.show()
