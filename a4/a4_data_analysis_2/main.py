#!/usr/bin/env python3

# Developer: Emmanuel Cruz

import functions as f

def main():
    f.get_requirements()
    f.demo()

if __name__=="__main__":
    main()